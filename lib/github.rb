require 'httparty'
require 'pry'

class Github
  include HTTParty
  base_uri "http://api.github.com"

  def repositories
    response = self.class.get('/repositories').body

    JSON.parse(response)
  end

end
