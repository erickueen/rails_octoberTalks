module Storable
  def store
    already_exists = self.class.exists?(gh_id: self.gh_id)

    already_exists ? self.class.find_by(gh_id: self.gh_id)
                    : self.class.create(self.attributes)
  end
end
